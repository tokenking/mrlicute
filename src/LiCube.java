/**
 * an cube
 * @author wangjian5@genomics.cn
 *
 */
public class LiCube {
	private String name; //
	
	public String getName() {
		return name;
	}

	public LiCube(String name)
	{
		this.name = name;
	}

	@Override
	public String toString() {
		return "LiCube [name=" + name + "]";
	}
	
	
}
